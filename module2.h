/* Example class definitions 
 * Ranga: 13/06/2019
 * */

#include"module1.h"

#ifndef SECOND_H
#define SECOND_H
//Concrete Child3 Class
class Child3: public Parent{
  public:
    void bar() const;
};
#endif
