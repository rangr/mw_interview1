/* Example class definitions 
 * Ranga: 13/06/2019
 * */
#ifndef FIRST_H
#define FIRST_H

//Abstract Parent Class
class Parent{

  public:
    void myfunc() const; //call myfunc_help()

    //function defined in parent class
    void foo() const;

    //function MUST be defined in child class: pure virtual function
    virtual void bar()const=0;

  protected:
    //helper function which can be overriden if needed
    virtual void myfunc_help() const;

};

//Concrete Child1 Class
class Child1: public Parent{
  public:
    void bar() const;
};


//Concrete Child2 Class
class Child2: public Parent{

  public:
    void bar() const;

  protected:
    //helper function which can be overriden if needed
   void myfunc_help() const;
};


//Concrete Child1 Sub-Class
class Child1_GC1: public Child1{
  public:
    void task() const;

};

//Concrete Child2 Sub-Class
class Child1_GC2: public Child1{
  public:
    void hardtask() const;
};

#endif
