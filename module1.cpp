/* Example class function implementation
 * Ranga: 13/06/2019
 * */

#include"module1.h"
#include<iostream>

//Parent functions definitions

void Parent::foo() const{
  std::cout<<"Parent::foo"<<std::endl;
}

void Parent::myfunc() const{
  myfunc_help();
}

void Parent::myfunc_help() const{
  std::cout<<"Parent::myfunc"<<std::endl;
}


//Child1 definitions

void Child1::bar() const{
  std::cout<<"Child1::bar"<<std::endl;
}


//Child2 functions
void Child2::bar() const{
  std::cout<<"Child2::bar"<<std::endl;
}

void Child2::myfunc_help() const{
  std::cout<<"Child2::myfunc"<<std::endl;
}


//Grand-GC1 task function
void Child1_GC1::task() const{
  std::cout<<"Child1_GC1::task"<<std::endl;
}

//Grand-GC1 task function
void Child1_GC2::hardtask() const{
  std::cout<<"Child1_GC2::hardtask"<<std::endl;
}

