/* Example inheritance code
 * Ranga: 13/06/2019
 * */
#include<iostream>
#include"module1.h"
#include"module2.h"


void print_prop(const std::string& input,const Parent& obj){

  std::cout<<input<<"'s foo: "; obj.foo();
  std::cout<<input<<"'s bar: "; obj.bar();
  std::cout<<input<<"'s myfunc: "; obj.myfunc();
}


int main(){
  Child1 C1;
  print_prop("Child 1",C1);
  std::cout<<std::endl;

  Child1_GC2 GC2;
  print_prop("Grandchild2",GC2);
  std::cout<<"Grandchild2's hard task: "; GC2.hardtask();
  std::cout<<std::endl;

  Child2 C2;
  print_prop("Child2",C2);
  std::cout<<std::endl;

  Child3 C3;
  print_prop("Child3",C3);
  std::cout<<std::endl;

  return 0;

}


