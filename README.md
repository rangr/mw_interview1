# README
An example program to test OOD concepts and inheritance

Image shows the class hierarchy that was desired.

My task was to add a new functionality **myfunc** that satisfies the following properties:
	* available for all sub-classes of the Parent, 
	* doesn't break any of the existing subclasses, and 
	* allows for changes in future derived classes.

I used a virtual function helper called **myfunc_help** to satisfy the above. To test if it works, I added a file containing new child class 3.


To test and run the program, please make and run *main.cpp*
