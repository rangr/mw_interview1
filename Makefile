# Declaration of variables
#program_FLAGS :=  -std=c++11 -O3 -march=native # speed
program_FLAGS :=-std=c++11 -Wshadow -Wall -Wextra -pedantic -g# debug 

CFLAGS += $(foreach includedir,$(program_INCLUDE_DIRS),-I$(includedir))
CFLAGS += $(program_FLAGS)
LDFLAGS += $(foreach librarydir,$(program_LIBRARY_DIRS),-L$(librarydir))
LDFLAGS += $(foreach library,$(program_LIBRARIES),-l$(library))

CC = g++
#CC_FLAGS = -std=c++11 -O3 -march=native

# File names
EXEC = main
SOURCES = main.cpp module1.cpp module2.cpp
#LDFLAGS= -lm -lgsl

OBJECTS = $(SOURCES:.cpp=.o)
 
# Main target
$(EXEC): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXEC) $(LDFLAGS)
 
# To obtain object files
%.o: %.cpp
	$(CC) -c $(CFLAGS) $< -o $@ $(LDFLAGS)
 
# To remove generated files
clean:
	rm -f $(EXEC) $(OBJECTS)
